const dbutil = require('../database/dbutil');
const SQL = require('sql-template-strings');


var permset = new Set();

function loadPermset( ) {

	dbutil.select(SQL`
		SELECT * FROM user_permissions;
	`).then((result) => {
		result.result.forEach((element) => {
			permset.add({ user: element.user, guild: element.guild });
		});
	}).then(() => { console.log(permset); });


}

loadPermset();

module.exports = {
	innerPerms : permset
};