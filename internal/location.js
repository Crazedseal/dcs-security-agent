const Discord = require('discord.js');

/* Location Object:
		name - string
		desc - string
		channel - Discord.Channel
		members - Discord.User[]
*/

const LOCATION_TYPES = {
	SUPERLOCATION: 0,
	NORMALLOCATION: 1,
	SUBLOCATION: 2,
	"0": "SUPERLOCATION",
	"1": "NORMALLOCATION",
	"2": "SUBLOCATION"
};


module.exports = {
	LOCATION_TYPES: LOCATION_TYPES
}



