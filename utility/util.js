function clamp(num, min, max) {
	if (num < min) { return min; }
	else if (num > max) { return max; }
	return num;
}

function validInteger(string) {
	try {
		parseInt(string);
		return true;
	}
	catch (err) {
		return false;
	}

}

async function getFirstPlayerFromMessage(message, args, where) {
	var return_result = { player: null, success: true, reason: "" };

	if (message.mentions.members.size > 0) {
		return_result.player = message.mentions.members.first();
	}
	else {
		const fetch_result = await message.guild.members.fetch({ query: args[where], limit: 1 } );
		const id_result = message.guild.members.resolve(args[where]);
		if (fetch_result.size < 1 && id_result == null) { 
			return_result.success = false;
			return_result.reason = `Could not find user: ${args[where]}`;
			return return_result;
		}
		else if (fetch_result.size > 0) {
			return_result.player = fetch_result.first();
		}
		else if (id_result != null) {
			return_result.player = id_result;
		}
		else {
			return_result.player = id_result;
		}
	}

	return return_result;
}

module.exports = {
	clamp: clamp,
	getFirstPlayerFromMessage: getFirstPlayerFromMessage,
	validInteger: validInteger
}