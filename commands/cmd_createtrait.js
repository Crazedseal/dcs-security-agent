const dbutil = require('../database/dbutil.js');
const SQL = require("sql-template-strings");

async function createtrait(message, args) {
	// 

	const trait_name = args[0];
	const trait_desc = args[1];

	const dbres = await dbutil.modify(SQL`
		INSERT INTO traits (name, guild, desc)
		VALUES (${trait_name}, ${message.guild.id}, ${trait_desc});
	`);

	if (!dbres.success) {
		message.reply(`Failed to insert trait into database. See console log for more details.\nA trait with the same name may already exist.`);
		return;
	}

	message.reply(`Successfully created trait ${args[0]} with description: ${args[1]}.`);
	return;
}

module.exports = {
	name: 'createtrait',
	description: 'Creates a trait with the description.',
	complete: false,
	guildonly: true,
	permission: 1,
	arguments: [
		"name", "description"
	],
	execute: createtrait
}