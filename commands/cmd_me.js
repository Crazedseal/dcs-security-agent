const dbutil = require('../database/dbutil.js');
const SQL = require('sql-template-strings');
const Discord = require('discord.js');

async function me(message, args) {
	var server_id;

	if (args.length > 0 || message.guild == null) {
		server_id = args[0];
	}
	else { server_id = message.guild.id; }

	const character_result = await dbutil.selectOne(SQL`
		SELECT * FROM character
		WHERE user = ${message.author.id} AND guild = ${server_id};
	`);

	if (!character_result.found) {
		message.reply(`You do not currently have a character attached to you at that server.`);
		return;
	}

	const character_location = await dbutil.selectOne(SQL`
		SELECT * FROM user_locations
		WHERE user = ${message.author.id} AND guild = ${server_id};
	`);

	const trait_assigned_results = await dbutil.select(SQL`
		SELECT * FROM traits_assigned
		WHERE character_name = ${character_result.result.name} AND guild = ${server_id};
	`);

	var traits = [];

	for (const value of trait_assigned_results.result) {
		const trait_result = await dbutil.selectOne(SQL`
			SELECT * FROM traits
			WHERE name = ${value.trait_name} AND guild = ${server_id};
		`);
		if (trait_result.found) {
			traits.push(trait_result.result);
		}
	}

	var embed = new Discord.MessageEmbed()
		.setTitle(character_result.result.name)
		.setDescription(
			`**Location:**\n${character_location.name}\n\n**Description:** \n${character_result.result.desc}`
		
		).addField(
			`Traits`, `------------------------------------------------`);

	
	traits.forEach(
		(value) => {
			embed = embed.addField(
				value.name,
				value.desc,
				true
			);
		}
	);

	message.author.send(embed);
	message.channel.send('Your character information has been sent to your private messages.');




}

module.exports = {
	name: 'me',
	description: 'Prints out your character information to your private messages. If given no argument on a server, it will give your character on the current server.',
	complete: false,
	guildonly: false,
	permission: 0,
	arguments: [
		"[o]serverid"
	],
	execute: me
}