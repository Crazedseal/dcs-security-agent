const dbutil = require('../database/dbutil.js');
const SQL = require("sql-template-strings");
const Discord = require('discord.js');
const util = require('../utility/util.js');
const fs = require('fs').promises;

async function dump(message, args) {
	var amount = util.clamp(args[0], 1, 5000); // 5000 messages.
	const location = await dbutil.selectOne(SQL`
		SELECT * FROM locations
		WHERE name = ${args[1]} AND guild = ${message.guild.id};
	`);

	var offset = 0;
	if (args.length > 2) {
		if (args[2] < 0) {
		}
		else {
			offset = args[2];
		}
	}

	if (!location.found) {
		message.reply(`Could not find location: ${args[1]}. Please check your spelling. (It is case senstive!)`);
		return;
	}

	const last_msg = await dbutil.select(SQL`
		SELECT * FROM location_messages
		WHERE guild = ${message.guild.id} AND location = ${args[1]}
		ORDER BY timestamp DESC
		LIMIT ${amount} OFFSET ${offset};
	`);

	if (!last_msg.found) {
		message.reply(`Unable to find any stored messages... Maybe an eror... Maybe a lack of messages.`);
		return;
	}

	var authors = { };
	var construct = "";

	var time = Date.now();

	for (var i = 0; i < last_msg.result.length; i++) {
		var currentMessageResult = last_msg.result[i];
		var currentMessageAuthor = last_msg.result[i].author;
		var currentMessageTime = new Date(currentMessageResult.timestamp).toUTCString();

		if (authors[currentMessageAuthor] == null) {
			authors[currentMessageAuthor] = await message.guild.members.fetch(last_msg.result[i].author);
		}
		construct = construct + `${authors[currentMessageAuthor].displayName} (${currentMessageResult.character}) - ${currentMessageTime}\n${currentMessageResult.content}\n\n`
	}

	try {
		await fs.writeFile(`dump${time}.txt`, construct);
	}
	catch (error) {
		console.error(error);
		message.reply(`An error occured during file writing.`);
		return;
	}

	await message.channel.send("Dump file: ", { 
		files: [
			`dump${time}.txt`
		]
	});

	fs.unlink(`dump${time}.txt`);

	return;

}


module.exports = {
	name: 'dump',
	description: 'Dumps the last [amount] of messages in a [location] to a text file and then is uploaded..',
	arguments: [
		"amount", "location", "[o]offset"
	],
	complete: false,
	guildonly: true,
	permission: 1,
	execute: dump
}
