const dbutil = require('../database/dbutil.js');
const SQL = require("sql-template-strings");

async function givetrait(message, args) {
	// 

	const trait_name = args[0];
	const character = args[1];

	var trait_result;
	var character_result;

	trait_result = await dbutil.selectOne(SQL`
		SELECT * FROM traits
		WHERE name = ${trait_name};
	`)

	character_result = await dbutil.selectOne(SQL`
		SELECT * FROM character
		WHERE name = ${character} AND guild = ${message.guild.id};
	`)

	if (trait_result.found == false && character_result.found == false) {
		message.reply(`Could not find trait: ${trait_name}. Could not find character: ${character}`);
		return;
	}
	else if (trait_result.found && character_result.found == false) {
		message.reply(`Could not find character: ${character}`);
		return;
	}
	else if (character_result.found && trait_result.found == false) {
		message.reply(`Could not find trait: ${trait_name}.`);
		return;
	}

	var dbresult = await dbutil.modify(SQL`
		INSERT INTO traits_assigned (trait_name, character_name, guild)
		VALUES (${trait_name}, ${character}, ${message.guild.id});
	`);

	if (!dbresult.success) {
		message.reply(`Unable to insert into database. See console for more details. That trait assignment may already exist.`);
		return;
	}

	message.reply(`Trait ${trait_name} successfully given to ${character}.`);
	return;
}

module.exports = {
	name: 'givetrait',
	description: 'Gives a trait to a character.',
	complete: false,
	guildonly: true,
	permission: 1,
	arguments: [
		"trait", "character"
	],
	execute: givetrait
}