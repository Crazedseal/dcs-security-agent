const dbutil = require('../database/dbutil.js');
const SQL = require("sql-template-strings");
const location = require('../internal/location.js');
const check = require('../check.js');

async function create(message, args) {
	var location_name = args[0];
	var location_desc = args[1];

	var location_type = args[2];
	var location_parent = args[3];

	var location_channel = message.channel;

	if (!check.isDefined(location_name)) {
		message.reply('Missing argument: name.');
		return;
	}
	if (!check.isDefined(location_desc)) {
		message.reply('Missing argument: desc.');
		return;
	}
	if (!check.isDefined(location_type)) {
		message.reply('Missing argument: type.');
		return;
	}

	const res = await dbutil.selectOne(SQL`SELECT * FROM locations WHERE name=${location_name} AND guild=${message.guild.id};`);
	if (res.found) {
		message.reply(`Cannot create location: ${location_name} as it already exists on this server.`);
		return;
	}

	// SUPERLOCATION
	if (location_type == location.LOCATION_TYPES.SUPERLOCATION || location_type.toLowerCase() == "superlocation") {
		// ignore channel.
		// ignore parent.

		const res = await dbutil.modify(
			SQL`INSERT INTO locations (name, guild, desc, channel, deletetime, autodelete, type, parent) VALUES
			(${location_name}, ${message.guild.id}, ${location_desc}, ${location_channel.id}, 30, 0, 0, NULL);`);

		if (!res.success) {
			message.reply(`Failed to insert location into database due to: ${res.reason}`);
			return;
		}
		
		message.reply(`Successfully created SUPERLOCATION ${location_name}`);
		return;
	}

	if (location_parent != null && location_parent != "") {
		const res = await dbutil.selectOne(SQL`SELECT * FROM locations WHERE name=${location_parent} AND guild=${message.guild.id};`)
		if (!res.found) {
			message.reply(`Cannot find parent location: ${location_parent}, command not successfully executed!`);
			return;
		}
	}

	const location_check = await dbutil.selectOne(SQL`SELECT * FROM locations WHERE channel=${message.channel.id};`);

	if (location_check.found) {
		message.reply('Channel already has a location attached!');
		return;
	}
	
	const location_insert = await dbutil.modify(SQL`
		INSERT INTO locations (name, desc, channel, guild, deletetime, autodelete, type) 
		VALUES(${location_name}, ${location_desc}, ${location_channel.id}, ${message.guild.id}, 30, ${false}, ${location_type});
	`);


	if (!location_insert.success) { 
		console.log(`Failed to insert into locations due to: ${result.reason}`);
		message.reply('An error occured in inserting this location into the database. See console for reason.');
		return;
	}

	console.log(`Successfully inserted ${location_name}, ${location_desc}, ${location_channel.id}, ${message.guild.id} into locations.`) 
	message.reply(`Successfully created location: ${location_name} with description ${location_desc} inside channel ${location_channel.name}.`)
	return;

}


module.exports = {
	name: 'create',
	description: 'Creates a location with specified name, description, type and parent.',
	complete: true,
	guildonly: true,
	permission: 1,
	arguments: [
		"name", "desc", "type", "[o]parent"
	],
	guildOnly: true,
	execute: create
}