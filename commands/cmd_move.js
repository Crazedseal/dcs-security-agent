const check = require('../check.js');
const dbutil = require('../database/dbutil.js');
const SQL = require("sql-template-strings");

async function move(message, args) {

	
	// Check the mentions
	if (check.isDefined(message.mentions.members.first())) {
		var member = message.mentions.members.first().user;
		move_step_2(message, args, member);
	}

	else {
		// Else attempt to resolve a username
		var user = message.guild.members.fetch({ query: args[0], limit: 1 } );
		user.then((members) => {
			if (members.size == 0) { 
				message.reply(`Cannot find user: ${args[0]}`);
				return; 
			}
			var member = members.entries().next().value[1].user;
			move_step_2(message, args, member);
		});
	}

}

async function move_step_2 (message, args, user) {

	// Resolve location.
	var previous;
	var current;
	dbutil.selectOne(SQL`
		SELECT * FROM locations WHERE name = ${args[1]} AND guild = ${message.guild.id};
	`).then((result) => {
		if (!result.found) {
			message.reply(`Cannot find location: ${args[1]} on ${message.guild.name}.`);
			throw 'Cannot find location.';
		}
		current = result.result;
		return dbutil.selectOne(SQL`
			SELECT * FROM user_locations WHERE user = ${user.id} AND guild = ${message.guild.id}; 
		`);

	}).then((result) => {
		if (result.found) {
			previous = result.result;
			return dbutil.modify(SQL` 
				DELETE FROM user_locations WHERE user = ${user.id} AND guild = ${message.guild.id};
			`).then((result) => { return { success: result.success, reason: result.reason, found: true }; });

		}
		else {
			return { found: false };
		}

	}).then((result) => {
		dbutil.modify(SQL` 
			INSERT INTO user_locations (name, user, guild, channel) 
			VALUES (${args[1]}, ${user.id}, ${message.guild.id}, ${current.channel});
		`);

		if (result.found) {
			var prevChannel = message.guild.channels.cache.get(previous.channel);

			prevChannel.overwritePermissions([
				{
					id: user.id,
					deny: ['VIEW_CHANNEL', 'SEND_MESSAGES', 'SEND_TTS_MESSAGES']
				}
			])
		}

		var curChannel = message.guild.channels.cache.get(current.channel);

		curChannel.overwritePermissions([
			{
				id: user.id,
				allow: ['VIEW_CHANNEL', 'SEND_MESSAGES', 'SEND_TTS_MESSAGES']
			}
		]);

		message.reply(`Moved ${user.username} into ${args[1]}.`)




	}).catch((error) => {
		console.error(error);
	});


}

module.exports = {
	name: 'move',
	description: 'Moves a player to a location.',
	permission: 1,
	guildonly: true,
	complete: false,
	arguments: [
		"player", "location"
	],
	execute: move
}