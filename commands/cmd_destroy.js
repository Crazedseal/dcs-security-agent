function destroy(message, args) {
	destroy_func[args.length](message, args);
}

async function destroy_args_0(message, args) {
	// Destroy this location and send players into the void.
	var channelID = message.channel.id;

	

}

async function destroy_args_1(message, args) {
	// Destroy the location and send players into the void.
}

async function destroy_args_2(message, args) {
	// Destroy the location and send players to the other location.
}

var destroy_func = [destroy_args_0, destroy_args_1, destroy_args_2];

module.exports = {
	name: 'destroy',
	description: 'Destroys a location. Players will be sent to limbo unless a second argument is given.',
	complete: false,
	guildonly: true,
	permission: 1,
	arguments: [
		"location", "location"
	],
	execute: destroy
}