function resetlocperms(message, args) {
	
	console.log('move called');
}

module.exports = {
	name: 'resetlocperms',
	description: 'Creates a channel between two or more players for messaging that is not in person.',
	complete: false,
	guildonly: true,
	permission: 1,
	arguments: [
		
	],
	execute: resetlocperms
}