async function walk(message, args) {


}

module.exports = {
	name: 'walk',
	description: 'Move your character to another sub-location',
	permission: 0,
	guildonly: true,
	complete: false,
	arguments: [
		"location"
	],
	execute: walk
}