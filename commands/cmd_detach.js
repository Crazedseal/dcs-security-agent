const dbutil = require('../database/dbutil.js');
const SQL = require("sql-template-strings");

async function detach(message, args) {
	const location = await dbutil.selectOne(SQL`
		SELECT * FROM locations
		WHERE guild = ${message.guild.id} AND channel = ${message.channel.id};
	`);

	if (!location.found) {
		message.reply(`There is no location attached to this channel!`);
		return;
	}

	const update_result = await dbutil.modify(SQL`
		UPDATE locations
		SET channel = 'none'
		WHERE guild = ${message.guild.id} AND channel = ${message.channel.id};
	`);

	if (!update_result.success) {
		message.reply(`There was an error detaching the channel in the database. Contact the bot owner.`);
		return;
	}
	message.reply(`Successfully detached ${location.result.name} from ${message.channel.name}.`);
	return;
	

}




module.exports = {
	name: 'detach',
	description: 'Detachs a location from this channel.',
	guildOnly: true,
	permission: 1,
	arguments: [
	],
	complete: true,
	execute: detach
}