
const dbutil = require('../database/dbutil.js');
const SQL = require('sql-template-strings');
const Discord = require('discord.js');
const util = require('../utility/util.js');

async function who(message, args) {
	var get_player = await util.getFirstPlayerFromMessage(message, args, 0);

	if (!get_player.success) {
		message.reply('Could not find user: ' + args[0]);
		return;
	}

	var target_user = get_player.player;
	var server_id;

	if (args.length > 1 || message.guild == null) {
		server_id = args[1];
	}
	else { server_id = message.guild.id; }

	const character_result = await dbutil.selectOne(SQL`
		SELECT * FROM character
		WHERE user = ${message.author.id} AND guild = ${server_id};
	`);

}


module.exports = {
	name: 'who',
	description: 'Prints out character information of player to your private messages. If given no argument on a server, it will give your character on the current server.',
	complete: false,
	guildonly: false,
	permission: 1,
	arguments: [
		"user", "[o]serverid"
	],
	execute: who
}