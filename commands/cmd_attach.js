const dbutil = require('../database/dbutil.js');
const SQL = require("sql-template-strings");

async function attach(message, args) {
	const location = await dbutil.selectOne(SQL`
		SELECT * FROM locations
		WHERE guild = ${message.guild.id} AND channel = ${message.channel.id};
	`);

	const this_location = await dbutil.selectOne(SQL`
		SELECT * FROM locations
		WHERE guild = ${message.guild.id} AND name = ${args[0]};
	`);

	if (!this_location.found) {
		message.reply(`Could not find location in database ${args[0]} on this server.`);
		return;
	}

	if (this_location.result.channel != "none" && this_location.result.channel != null) {
		const channelFetch = message.guild.channels.resolve(this_location.result.channel);
		if (channelFetch != null) {
			message.reply(`The location ${args[0]} is already attached to a channel: ${channelFetch.name}`);
			return;
		}
	}

	if (location.found && (args[1] == null || args[1].toLowerCase() != "-o")) {
		message.reply(`There is already location attached to this channel!`);
		return;
	}

	const update_result = await dbutil.modify(SQL`
		UPDATE locations
		SET channel = ${message.channel.id}
		WHERE guild = ${message.guild.id} AND name = ${args[0]};
	`);

	if (!update_result.success) {
		message.reply(`There was an error detaching the channel in the database. Contact the bot owner.`);
		return;
	}
	message.reply(`Successfully attached ${args[0]} to ${message.channel.name}.`);
	return;
}




module.exports = {
	name: 'attach',
	description: 'Attachs a location to this channel. -o to allow it to attach to this channel even if the channel has a location attached already. Example Usage:\n>>attach "DCS Headquarters"\n>>attach "DCS Headquarters" -o',
	guildOnly: true,
	permission: 1,
	arguments: [
		"location", "[o]-o"
	],
	complete: true,
	execute: attach
}