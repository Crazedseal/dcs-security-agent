const dbutil = require('../database/dbutil.js');
const SQL = require("sql-template-strings");

async function makechar(message, args) {
	if (args.length == 3) {
		if (args[1].toLowerCase() == "-p") {
			const player = await getFirstPlayerFromMessage(message, args, 2);

			if (!player.success) {
				message.reply(player.reason);
				return;
			}

			const dbresult = await dbutil.modify(SQL`
				INSERT INTO character (name, guild, user, desc) VALUES
				(${args[0]}, ${message.guild.id}, ${player.player.id}, NULL);
			`);

			if (!dbresult.success) {
				message.reply(`Was unable to insert into database. Check console log for details.`);
				return;
			}

			message.reply(`Made character ${args[0]} with player ${player.displayName}.`);
			return;
		}
		else if (args[1].toLowerCase() == "-d") {

			const dbresult = await dbutil.modify(SQL`
				INSERT INTO character (name, guild, user, desc) VALUES
				(${args[0]}, ${message.guild.id}, NULL, ${args[2]});
			`);

			if (!dbresult.success) {
				message.reply(`Was unable to insert into database. Check console log for details.`);
				return;
			}

			message.reply(`Made character ${args[0]} with desc: ${args[2]}.`);
			return;
		}
		else {
			const player = await getFirstPlayerFromMessage(message, args, 1);

			if (!player.success) {
				message.reply(player.reason);
				return;
			}

			const dbresult = await dbutil.modify(SQL`
				INSERT INTO character (name, guild, user, desc) VALUES
				(${args[0]}, ${message.guild.id}, ${player.player.id}, ${args[1]});
			`);

			if (!dbresult.success) {
				message.reply(`Was unable to insert into database. Check console log for details.`);
				return;
			}

			message.reply(`Made character ${args[0]} with player ${player.player.displayName} and desc: ${args[1]}`);
			return;
		}
	}
	else if (args.length == 1) {

		const dbresult = await dbutil.modify(SQL`
			INSERT INTO character (name, guild, user, desc) VALUES
			(${args[0]}, ${message.guild.id}, NULL, NULL);
		`);

		if (!dbresult.success) {
			message.reply(`Was unable to insert into database. Check console log for details.`);
			return;
		}

		message.reply(`Made character ${args[0]}.`);
		return;	

	}

}


async function getFirstPlayerFromMessage(message, args, where) {
	var return_result = { player: null, success: true, reason: "" };

	if (message.mentions.members.size > 0) {
		return_result.player = message.mentions.members.first();
	}
	else {
		const fetch_result = await message.guild.members.fetch({ query: args[where], limit: 1 } );
		if (fetch_result.size < 1) { 
			return_result.success = false;
			return_result.reason = `Could not find user: ${args[where]}`;
			return return_result;
		}
		return_result.player = fetch_result.first();
	}

	return return_result;
}

module.exports = {
	name: 'makechar',
	description: 'Makes a character with the specified name.',
	complete: false,
	guildonly: true,
	permission: 1,
	arguments: [
		"name", "[o]desc", "[o]player"
	],
	execute: makechar
}