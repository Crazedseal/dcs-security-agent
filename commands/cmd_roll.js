var seedrandom = require('seedrandom');
const config = require('../config.json');
const dbutil = require('../database/dbutil');
const SQL = require('sql-template-strings');

var rng = seedrandom();


const STEP_BUILD_NUMBER = 0;
const STEP_BUILD_TYPE = 1;
const STEP_BUILD_MODIFIER = 2;

async function roll(message, args) {
	var roll_channel;
	var dice_result = parseDice(args[0]);
	var roll_result = get_roll_result(dice_result);

	if (args[1] == undefined) {
		args[1] = "No reason given.";
	}

	if (config.hiddenrolls) {
		const roll_channel_result = await dbutil.selectOne(SQL`
			SELECT * FROM roll_channels
			WHERE guild = ${message.guild.id};
		`);

		if (!roll_channel_result.found) {
			message.reply('There is no roll channel on this server. This bot is currently set hiddenrolls to true.');
			return;
		}

		roll_channel = message.guild.channels.resolve(roll_channel_result.result.channel);
		if (roll_channel == null) {
			message.reply('The roll channel on this server may have been deleted. Please set up a different channel.');
			return;
		}

		roll_channel.send(`<@${message.member.id}> rolled (${args[0]}).\nResult: ${roll_result.total}. Rolls: [${roll_result.rolls}].\n${args[1]} in <#${message.channel.id}>.`, {"allowedMentions": { "users" : []}});
		return;
	}

	message.channel.send(`${message.member.displayName} rolled ${roll_result.total} | ${roll_result.rolls} - ${args[1]}`);
	return;


	
}

function get_roll_result(dice) {
	var results = {
		total: dice.modifier,
		rolls: []
	}
	for (var i = 0; i < dice.number; i++) {
		var croll = Math.floor(rng() * dice.type) + 1;
		results.total += croll;
		results.rolls.push(croll);
	}
	return results;
}

function parseDice(diceString) {
	var returnDice = {
		success: true,
		reason: "",
		step: "",
		number: 0,
		type: 0,
		modifier: 0
	}

	var step = 0;
	var currentNumber = "";
	var currentType = "";
	var currentModifier = "";
	var positiveModifier = true;

	const cleanDiceString = diceString.trim().replace(/\s+/g, '');

	var loopBreak = false;
	for (var i = 0; i < cleanDiceString.length; i++) {
		var currentChar = cleanDiceString.charAt(i);
		switch (step) {

			case STEP_BUILD_NUMBER:
				if (isDigit(currentChar)) {
					currentNumber = currentNumber + currentChar;
				}
				else if (currentChar.toLowerCase() == 'd') {
					step = STEP_BUILD_TYPE;
					continue;
				}
				else {
					returnDice.success = false;
					returnDice.step = "STEP_BUILD_NUMBER";
					returnDice.reason = "Malformed dice expression.";
					return returnDice;
				}
				break;
			case STEP_BUILD_TYPE:
				if (isDigit(currentChar)) {
					currentType = currentType + currentChar;
				}
				else if (currentChar.toLowerCase() == '+') {
					step = STEP_BUILD_MODIFIER;
					continue;
				}
				else if (currentChar.toLowerCase() == '-') {
					positiveModifier = false;
					step = STEP_BUILD_MODIFIER;
					continue;
				}
				else {
					loopBreak = true;
					returnDice.success = false;
					returnDice.step = "STEP_BUILD_TYPE";
					returnDice.reason = "Malformed dice expression.";
					return returnDice;
				}
				break;
			case STEP_BUILD_MODIFIER:
				if (isDigit(currentChar)) {
					currentModifier = currentModifier + currentChar;
				}
				else {
					loopBreak = true;
				}
				break;
		}

		if (loopBreak) { break; }

	}



	returnDice.number = currentNumber != "" ? parseInt(currentNumber) : 1;
	returnDice.type = parseInt(currentType);
	
	if (currentModifier != '') {
		returnDice.modifier = parseInt(currentModifier);

		if (!positiveModifier) { returnDice.modifier *= -1; }
	}

	return returnDice;



}

const digitTest = /\d/i
function isDigit(char) {
	return digitTest.test(char);
}



module.exports = {
	name: 'roll',
	description: 'Creates a channel between two or more players for messaging that is not in person.',
	complete: false,
	guildonly: true,
	permission: 0,
	arguments: [
		"dice", "[o]reason"
	],
	execute: roll
}