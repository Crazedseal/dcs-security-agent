function editloc(message, args) {
	
	console.log('move called');
}

module.exports = {
	name: 'editloc',
	description: `Edit's a [location]'s [property](-d for DESC, -t for TYPE) with [value].`,
	complete: false,
	guildonly: true,
	permission: 1,
	arguments: [
		"location", "property", "value"
	],
	execute: editloc
}