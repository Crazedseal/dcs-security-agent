const dbutil = require('../database/dbutil.js');
const SQL = require("sql-template-strings");


async function bot_perm(message, args) {
	var where = message.guild.id;

	if (message.mentions.members.size > 0) {
		// Assume member
		var who = message.mentions.members.first();

		const exists = await dbutil.selectOne(SQL`SELECT * FROM user_permissions WHERE user=${who.id} AND guild=${where};`);
		console.log(exists);

		if (exists.found) {
			const result = await dbutil.modify(SQL`
				DELETE FROM user_permissions WHERE user=${who.id} AND guild=${where};
			`);
			if (result.success) {
				message.reply(`Successfully revoked elevated privileges to ${who.username}`);
			}
			else {
				message.reply('Failed to perform action on database. See console log.');
				console.log(result.reason);
			}
			return;
		}

		const dbres = await dbutil.modify(SQL`
			INSERT INTO user_permissions (user, guild) VALUES
			(${who.id}, ${where});
		`);

		if (!dbres.success) {
			message.reply(`Unable to insert into database. See console logs for more details.`);
			return;
		}

		message.reply(`Successfully granted elevated privileges to ${who.username}`);
		return;
	}

	if (message.mentions.roles.size > 0) {
		var role = message.mentions.roles.first();

		const r_exists = await dbutil.selectOne(SQL`SELECT * FROM user_permissions WHERE user=${role.id} AND guild=${where};`).found;

		if (r_exists) {
			const r_result = await dbutil.modify(SQL`
				DELETE FROM user_permissions WHERE user=${role.id} AND guild=${where};
			`);
			if (r_result.success) {
				message.reply(`Successfully revoked elevated privileges to ${who.username}`);
			}
			else {
				message.reply('Failed to perform action on database. See console log.');
				console.log(r_result.reason);
			}
			return;
		}

		const dbres = dbutil.modify(SQL`
			INSERT INTO user_permissions (user, guild) VALUES
			(${role.id}, ${where});
		`);

		if (!dbres.success) {
			message.reply(`Unable to insert into database. See console logs for more details.`);
			return;
		}

		message.reply(`Successfully granted elevated privileges to ${role.name}`);
		return;
	}
	
}

module.exports = {
	name: 'bot',
	description: 'Toggles permissions to that role or user to use privileged commands.',
	complete: false,
	permission: 2,
	arguments: [
		"role/user"
	],
	execute: bot_perm
}