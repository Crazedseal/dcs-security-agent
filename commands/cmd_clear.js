function clear(message, args) {

	var author = message.member;
	var channel = message.channel;
	var number = args[0];

	channel.messages.fetch({ limit: number}).then((messages) => {
		messages.each(
			msg => channel.messages.delete(msg)
		);
	});

}




module.exports = {
	name: 'clear',
	description: 'Clears a channel of [x] newest messages.',
	guildOnly: true,
	permission: 1,
	arguments: [
		"amount"
	],
	complete: true,
	execute: clear
}