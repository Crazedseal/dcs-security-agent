const dbutil = require("../database/dbutil");

async function makeparent(message, args) {
	var loc_child = args[0];
	var loc_parent = args[1];

	const loc_child_exist = await dbutil.selectOne(SQL`
		SELECT * FROM locations
		WHERE name = ${loc_child} AND guild = ${message.guild.id};
	`);

	const loc_parent_exist = await dbutil.selectOne(SQL`
		SELECT * FROM locations
		WHERE name = ${loc_child} AND guild = ${message.guild.id};
	`);

	if (!loc_child_exist.found && !loc_parent_exist.found) {
		message.reply(`Location ${loc_child} does not exist. Location ${loc_parent} does not exist. Check your spelling.`);
		return;
	}
	else if (loc_child_exist.found && !loc_parent_exist.found) {
		message.reply(`Location ${loc_parent} does not exist. Check your spelling.`);
		return;
	}
	else if (!loc_child_exist.found && loc_parent_exist.found) {
		message.reply(`Location ${loc_child} does not exist. Check your spelling.`);
		return;
	}

	if (loc_child_exist.result.type >= loc_parent_exist.result.type) {
		message.reply(`Location ${loc_child} is a higher or equal type to ${loc_parent}.`);
		return;
	}

	const child_current_parent = await dbutil.selectOne(SQL`
		SELECT * FROM locations_hierarchy
		WHERE child = ${loc_child} AND guild = ${message.guild.id};
	`);

	if (child_current_parent.found) {
		const delete_result = await dbutil.modify(SQL`
			DELETE FROM locations_hierarchy
			WHERE child = ${loc_child} AND guild = ${message.guild.id};
		`);
		if (!delete_result.success) {
			message.channel.send(`Failed to delete parent-child relationship for ${loc_child}. Alert the bot owner.`);
		}
	}

	const insert_result = await dbutil.modify(SQL`
		INSERT INTO locations_hierarchy (parent, guild, child)
		VALUES (${loc_parent}, ${message.guild.id}, ${loc_child});
	`);

	if (!insert_result.success) {
		message.reply(`Was unable to insert child-parent relationship into database. Contact bot owner.`);
		return;
	}

	message.reply(`Successfully inserted child (${loc_child}) parent(${loc_parent}).`);
	return;
}

module.exports = {
	name: 'makeparent',
	description: 'Makes a location the child location of the parent location.',
	complete: false,
	guildonly: true,
	permission: 1,
	arguments: [
		"location-child", "location-parent"
	],
	execute: makeparent
}