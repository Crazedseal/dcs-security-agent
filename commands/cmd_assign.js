const dbutil = require("../database/dbutil");
const SQL = require("sql-template-strings");

async function assign(message, args) {
	var player;

	const character = await dbutil.selectOne(SQL`
		SELECT * FROM character
		WHERE name = ${args[0]};
	`);

	if (!character.found) {
		message.reply(`Could not find character ${args[0]}`);
		return;
	}

	if (message.mentions.members.size > 0) {
		player = message.mentions.members.first();
	}
	else {
		const fetch_result = await message.guild.members.fetch({ query: args[1], limit: 1 } );
		if (fetch_result.size < 1) { message.reply(`Could not find user: ${args[1]}`); return; }
		player = fetch_result.first();
	}

	const assign_result = await dbutil.modify(SQL`
		UPDATE character
		SET user = ${player.id}
		WHERE name = ${args[0]};
	`);

	if (assign_result.success) {
		message.reply(`Successfully assigned ${args[1]} to ${args[0]}`);
		return;
	}
	else {
		message.reply(`Was unable to assign ${args[1]} to ${args[0]} due to a database error.`);
		return;
	}

}




module.exports = {
	name: 'assign',
	description: 'Assigns a character to a user.',
	guildOnly: true,
	permission: 1,
	arguments: [
		"character", "user"
	],
	complete: false,
	execute: assign
}