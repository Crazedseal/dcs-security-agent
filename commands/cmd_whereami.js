const dbutil = require('../database/dbutil.js');
const SQL = require("sql-template-strings");

function whereami(message, args) {

	dbutil.selectOne(SQL`
		SELECT * FROM user_locations WHERE user = ${message.author.id} AND guild = ${message.guild.id};
	`).then((result) => {
		if (result.found) {
			return dbutil.selectOne(
				SQL`SELECT * FROM locations WHERE name = ${result.result.name} AND guild = ${message.guild.id};`
			);
		}
		else {
			message.reply("You are not in any location.");
			return { found: false };
		}
	}).then((result) => {

		if (!result.found) { return; }

		if (args.length > 0) {
			if (args[0].toLowerCase() == 'long') {
				message.reply(`You are currently located at ${result.result.name}.\nDescription: ${result.result.desc}`);
			}
			else if (args[0].toLowerCase() == 'short') {
				message.reply(`You are currently located at ${result.result.name}.`);
			}
			else {
				message.reply(`You are currently located at ${result.result.name}. Unknown argument: ${args[0]}`);
			}
		}
		else {
			message.reply(`You are currently located at ${result.result.name}.`);
		}
	});



}

module.exports = {
	name: 'whereami',
	description: 'Replies with the players current location.',
	arguments: [],
	complete: true,
	guildonly: true,
	permission: 0,
	execute: whereami
}