const dbutil = require('../database/dbutil');
const SQL = require('sql-template-strings');

async function makeroll(message, args) {

	const roll_channel_result = await dbutil.selectOne(SQL`
		SELECT * FROM roll_channels
		WHERE guild = ${message.guild.id};
	`);

	if (roll_channel_result.found) {
		const remove_old = await dbutil.modify(SQL`
			DELETE FROM roll_channels
			WHERE guild = ${message.guild.id};
		`);

		if (!remove_old.success) {
			message.reply('Failed to remove old roll channel from the database. Contact the bot owner.');
			return;
		}

		message.reply(`Successfully unregistered old roll channel for ${message.guild.name}.`);
	}

	const roll_channel_add = await dbutil.modify(SQL`
		INSERT INTO roll_channels (channel, guild)
		VALUES (${message.channel.id}, ${message.guild.id})
	`);

	if (!roll_channel_add.success) {
		message.reply('Unable to insert channel into database. Contact the bot owner.');
		return;
	}

	message.reply(`Successfully made ${message.channel.name} the roll channel for ${message.guild.name}.`);
	return;

}

module.exports = {
	name: 'makeroll',
	description: 'Makes this channel the servers roll channel.',
	complete: false,
	guildonly: true,
	permission: 1,
	arguments: [
		
	],
	execute: makeroll
}