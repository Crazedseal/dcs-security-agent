const dbutil = require('../database/dbutil.js');
const SQL = require("sql-template-strings");
const Discord = require('discord.js');
const util = require('../utility/util.js');

async function last_messages(message, args) {

	var amount = util.clamp(args[0], 1, 20);
	var location_name = args[1];
	var offset = 0;
	if (args.length > 2) {
		if (args[2] < 0) {
			offset = 0;
		}
		else {
			offset = args[2];
		}
	}

	const location_result = await dbutil.selectOne(SQL`
		SELECT * FROM locations 
		WHERE name = ${location_name} AND guild = ${message.guild.id};
	`);

	if (!location_result.found) {
		message.reply(`Could not find location: ${args[1]}`);
		return;
	}

	const last_msg = await dbutil.select(SQL`
		SELECT * FROM location_messages
		WHERE guild = ${message.guild.id} AND location = ${location_name}
		ORDER BY timestamp DESC
		LIMIT ${amount} OFFSET ${offset};
	`);

	var embed = new Discord.MessageEmbed();
	embed.setTitle(`${location_name}`)
	var authors = {};
	var charLength = 0;
	for (var i = last_msg.result.length - 1; i != 0; i--) {
		if (authors[last_msg.result[i].author] == null) {
			authors[last_msg.result[i].author] = await message.guild.members.fetch(last_msg.result[i].author);
		}
		const time = new Date(last_msg.result[i].timestamp).toUTCString();
		charLength = charLength + time.length + last_msg.result[i].content.length;

		if (charLength > 6000) {
			message.channel.send(embed);
			var embed = new Discord.MessageEmbed();
			embed.setTitle(`${location_name}`)
		}

		embed.addField(`User: ${authors[last_msg.result[i].author].displayName} Time: ${time}`, `${last_msg.result[i].content}`);

	}

	message.channel.send(embed);

	console.log(last_msg.result);

}

module.exports = {
	name: 'last',
	description: 'Posts the last [amount] of messages in a [location].',
	arguments: [
		"amount", "location", "[o]start"
	],
	complete: false,
	guildonly: true,
	permission: 1,
	execute: last_messages
}