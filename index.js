const Discord = require('discord.js');
const token = require('./token.json');
const config = require('./config.json');
const msgutil = require('./msg_util.js');
const db = require('./database/dbutil.js');
const dbmanage = require('./database/dbmanage.js');
const SQL = require("sql-template-strings");
var fs = require('fs');
const dbutil = require('./database/dbutil.js');

const prefix = config.prefix;

const client = new Discord.Client();
console.log("Client Created");

var permlevels = [ "None", "Elevated", "Administrator", "Bot Owner"]

var reloadCommand = {
	name: "reloadcommands",
	description: "Reloads the commands dynamically.",
	permission: 3,
	guildonly: true,
	complete: false,
	arguments: [],
	execute: reloadCommandFunc
}

var printcmd = {
	name: 'printcommands',
	description: 'Prints all of the available commands',
	complete: false,
	guildonly: false,
	permission: 0,
	arguments: [
		"[o]permlevel"
	],
	execute: printcommandsFunc
}


function reloadCommandFunc() {
	// Load Commands
	client.commands = new Discord.Collection();
	client.commands.set(reloadCommand.name, reloadCommand);
	client.commands.set(printcmd.name, printcmd);
	const commandFiles = fs.readdirSync('./commands').filter(file => file.endsWith('.js'));
	console.log("Loading commands.");
	for (const file of commandFiles) {
		console.log(`Loading: ${file}`);
		const command = require(`./commands/${file}`);
		client.commands.set(command.name, command);
	}
}


function printcommandsFunc(message, args) {
	var build = "";

	var embed = new Discord.MessageEmbed();
	var plevel = args[0];

	Promise.all(
		client.commands.each(
			(cmd) => {
				return new Promise(() => {
					if (cmd.permission == 3 || cmd.permission > plevel) { }
					else {
						embed.addField(`Name: ${cmd.name} [${cmd.complete ? "Complete" : "Incomplete"}]`, `Description: ${cmd.description}\nArguments: ${cmd.arguments}\nPermission: ${permlevels[cmd.permission]}`);
					}
				});
			}
		)
	).then(() => {
		
		message.channel.send(embed); 
	});
	
}

async function check_permission(message, command) {
	var author = message.member;
	var guild = message.guild;


	if (command.permission == 0 || author.id == config.owner) { return true; }
	if (author.hasPermission('ADMINISTRATOR')) { return command.permission <= 2; }

	var perms = await db.selectOne(SQL`
		SELECT * FROM user_permissions WHERE user = ${message.author.id} AND guild = ${message.guild.id}; 
	`);

	if (perms.found) {
		return command.permission <= 1;
	}
	else { 
		var found = false;

		await Promise.all(
			author.roles.cache.map(async (value) => {
				const r_result = await db.selectOne(SQL`
					SELECT * FROM user_permissions WHERE user = ${value.id} AND guild = ${message.guild.id}; 
				`);
				if (r_result.found) { found = true; }
			})
		);

		if (found) { return command.permission <= 1; }

		return command.permission == 0; 
	}
}

dbmanage.initdb().then((value) => {

	// Load Commands
	client.commands = new Discord.Collection();
	client.commands.set(reloadCommand.name, reloadCommand);
	client.commands.set(printcmd.name, printcmd);
	const commandFiles = fs.readdirSync('./commands').filter(file => file.endsWith('.js'));
	console.log("Loading commands.");
	for (const file of commandFiles) {
		console.log(`Loading: ${file}`);
		const command = require(`./commands/${file}`);
		client.commands.set(command.name, command);
	}

	client.on('ready', () => {
		console.log('Client ready');
		if (config.owner != null && config.owner != "") {
			client.users.fetch(config.owner).then(
				owner => console.log(`Owner found: ${owner.username}`)
			);
		}
	});


	// Handle Messages
	client.on('message', async (msg) => {
		// Check the message is a command, not from a bot and inside a discord server.
		if (msg.author.bot) { return; }
		if (!msg.content.startsWith(prefix) && msg.guild != null) { 
			
			const location = await dbutil.selectOne(SQL`
				SELECT * FROM locations
				WHERE channel = ${msg.channel.id} AND guild = ${msg.guild.id};
			`);

			const character = await dbutil.selectOne(SQL`
				SELECT * FROM character
				WHERE guild = ${msg.guild.id} AND user = ${msg.author.id};
			`);
			
			if (!location.found) { return; }

			const msg_author = msg.author.id;
			const msg_id = msg.id;
			const msg_content= msg.content;
			const msg_time = Date.now();
			const msg_guild = msg.guild.id;
			
			if (!character) {
				const msg_log_result = await dbutil.modify(SQL`
					INSERT INTO location_messages (messageid, guild, location, timestamp, content, author, character)
					VALUES (${msg_id}, ${msg_guild}, ${location.result.name}, ${msg_time}, ${msg_content}, ${msg_author}, NULL);
				`);

				if (!msg_log_result.success) {
					console.error(`Could not insert into database (location_messages) - (${msg_id}, ${msg_guild}, ${location.result.name}, ${msg_time}, ${msg_content}, ${msg_author}. NULL)`);
					return;
				}
			}
			else {
				const msg_log_result = await dbutil.modify(SQL`
					INSERT INTO location_messages (messageid, guild, location, timestamp, content, author, character)
					VALUES (${msg_id}, ${msg_guild}, ${location.result.name}, ${msg_time}, ${msg_content}, ${msg_author}, ${character.result.name});
				`);

				if (!msg_log_result.success) {
					console.error(`Could not insert into database (location_messages) - (${msg_id}, ${msg_guild}, ${location.result.name}, ${msg_time}, ${msg_content}, ${msg_author}. ${character.result.name})`);
					return;
				}
			}
			
			return;
		}

		var m_result = msgutil.processMessage(prefix, msg.content);

		if (!client.commands.has(m_result.command)) { 
			msg.reply("Unknown command: " + m_result.command);
			return; 
		}

		const cmd = client.commands.get(m_result.command);

		if (cmd.guildonly && msg.guild == null) {
			msg.reply("You need to be in a server to use this command.");
			return;
		}

		const perm = check_permission(msg, cmd);
		perm.then((result) => {
			if (!result) { msg.reply('You do not have the required permissions to do that command.'); return; }


			try {
				client.commands.get(m_result.command).execute(msg, m_result.arguments);
			}
			catch (error) {
				console.error(error);
				msg.reply("There was an error in attempting to execute the command.");
		}
		});

	});


	client.login(token.value);
});