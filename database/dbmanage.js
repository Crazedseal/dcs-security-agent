const dbutil = require('./dbutil.js');
const SQL = require("sql-template-strings");

async function initdb() {

	console.log("Checking for 'locations' table.");
	const locations_exist = await dbutil.tableExists('locations');
	if (!locations_exist) { 
		console.log("Table 'locations' does not exist. Creating it.");
		const locations = await dbutil.createTable(SQL`
			CREATE TABLE locations (
				name TEXT NOT NULL,
				guild TEXT NOT NULL,
				desc TEXT NOT NULL,
				channel TEXT NOT NULL,
				deletetime INTEGER NOT NULL,
				autodelete BOOLEAN NOT NULL,
				type INTEGER NOT NULL,
				parent TEXT,
				accessible BOOLEAN NOT NULL,
				PRIMARY KEY (name, guild),
				FOREIGN KEY (parent) REFERENCES locations(name)
			);
		`);
	}

	console.log("Checking for 'locations_hierarchy' table.");
	const locations_hierarchy_exist = await dbutil.tableExists('locations_hierarchy');
	if (!locations_hierarchy_exist) { 
		console.log("Table 'locations_hierarchy' does not exist. Creating it.");
		const locations = await dbutil.createTable(SQL`
			CREATE TABLE locations_hierarchy (
				parent TEXT NOT NULL,
				guild TEXT NOT NULL,
				child TEXT NOT NULL,
				PRIMARY KEY (child, guild),
				FOREIGN KEY (parent) REFERENCES locations(name),
				FOREIGN KEY (child) REFERENCES locations(name)
			);
		`);
	}

	console.log("Checking for 'user_locations' table.");
	const user_locations_exist = await dbutil.tableExists('user_locations');
	if (!user_locations_exist) {
		console.log("Table 'user_locations' does not exist. Creating it.");
		const user_locations = await dbutil.createTable(SQL`
			CREATE TABLE user_locations (
				name TEXT NOT NULL,
				user TEXT NOT NULL,
				guild TEXT NOT NULL,
				channel TEXT NOT NULL,
				PRIMARY KEY (name, user, guild),
				FOREIGN KEY (name) REFERENCES locations(name)
			);
		`);
	}

	console.log("Checking for 'user_permissions' table.");
	const user_permissions_exist = await dbutil.tableExists('user_permissions');
	if (!user_permissions_exist) {
		console.log("Table 'user_permissions' does not exist. Creating it.");
		const user_permissions = await dbutil.createTable(SQL`
			CREATE TABLE user_permissions (
				user TEXT NOT NULL,
				guild TEXT NOT NULL,
				PRIMARY KEY (user, guild)
			);
		`);
	}

	console.log("Checking for 'character' table.");
	const character_exists = await dbutil.tableExists('character');
	if (!character_exists) {
		console.log("Table 'character' does not exist. Creating it.");
		const character = await dbutil.createTable(SQL`
			CREATE TABLE character (
				name TEXT NOT NULL,
				guild TEXT NOT NULL,
				user TEXT,
				desc TEXT,
				PRIMARY KEY (name, guild),
				UNIQUE (guild, user)
			);
		`);
	}

	
	console.log("Checking for 'traits' table.");
	const traits_exist = await dbutil.tableExists('traits');
	if (!traits_exist) {
		console.log("Table 'traits' does not exist. Creating it.");
		const traits = await dbutil.createTable(SQL`
			CREATE TABLE traits (
				name TEXT NOT NULL,
				guild TEXT NOT NULL,
				desc TEXT,
				PRIMARY KEY (name, guild)
			);
		`);
	}

	console.log("Checking for 'traits_assigned' table.");
	const traits_assigned_exist = await dbutil.tableExists('traits_assigned');
	if (!traits_assigned_exist) {
		console.log("Table 'traits_assigned' does not exist. Creating it.");
		const traits_assigned = await dbutil.createTable(SQL`
			CREATE TABLE traits_assigned (
				trait_name TEXT NOT NULL,
				character_name TEXT NOT NULL,
				guild TEXT NOT NULL,
				PRIMARY KEY (trait_name, character_name, guild),
				FOREIGN KEY (trait_name) REFERENCES traits(name),
				FOREIGN KEY (character_name, guild) REFERENCES character(name, guild)
			);
		`);
	}

	console.log("Checking for 'location_messages' table.");
	const location_messages_exist = await dbutil.tableExists('location_messages');
	if (!location_messages_exist) {
		console.log("Table 'location_messages' does not exist. Creating it.");
		const location_messages = await dbutil.createTable(SQL`
			CREATE TABLE location_messages (
				messageid TEXT NOT NULL,
				guild TEXT NOT NULL,
				location TEXT NOT NULL,
				timestamp INTEGER NOT NULL,
				content TEXT NOT NULL,
				author TEXT NOT NULL,
				character TEXT,
				PRIMARY KEY (messageid),
				FOREIGN KEY (location, guild) REFERENCES locations(name, guild)
			);
		`);
	}

	console.log("Checking for 'roll_channels' table.");
	const roll_channels_exist = await dbutil.tableExists('roll_channels');
	if (!roll_channels_exist) {
		console.log("Table 'roll_channels' does not exist. Creating it.");
		const roll_channels = await dbutil.createTable(SQL`
			CREATE TABLE roll_channels (
				channel TEXT NOT NULL,
				guild TEXT NOT NULL,
				PRIMARY KEY (guild)
			);
		`);
	}


}

module.exports = {
	initdb: initdb
}