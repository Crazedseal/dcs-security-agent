function processMessage(prefix, message) {
	var result = {
		status: "ok",
		reason: "",
		command: "",
		arguments: []
	}

	var msg = message;
	msg = msg.slice(prefix.length);
	var escaped = [];
	var finding_command = true;
	var current_build = "";
	var building_value = false;
	var building_string = false;

	for (var i = 0; i < msg.length; i++) {
		var character = msg[i];
		
		if (finding_command) {

			if (character == ' ') { finding_command = false; }
			else { result.command = result.command + character; }

		}
		else {
			if (building_string) {
				if (character == '"' && !escaped[i]) {
					building_string = false;
					result.arguments.push(current_build);
					current_build = "";
				}
				else if (character == '"') {
					current_build = current_build + character;
				}
				else if (character == '\\' && !escaped[i]) {
					escaped[i+1] = true;
				}
				else if (character == '\\') {
					current_build = current_build + character;
				}
				else {
					current_build = current_build + character;
				}

			}
			else {
				if (character == ' ' && building_value) {
					if (current_build == "") { continue; }
					result.arguments.push(current_build);
					current_build = "";
				}
				else if (character == '"') {
					building_string = true;
				}
				else if (character == ' ' && !building_value) {
					continue;
				}
				else {
					if (!building_value) { building_value = true; }
					current_build = current_build + character;
				}
			}

		}
	}
	if (current_build != "") {
		result.arguments.push(current_build);
	}


	return result;
}

exports.processMessage = processMessage;